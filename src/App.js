// App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Note the change in import
import Banner from './components/Banner';
import Experts from './components/Experts';
import Footer from './components/Footer';
import Header from './components/Header';
import NewsLetter from './components/NewsLetter';
import Plans from './components/Plans';

function App() {
  return (
    <Router>
      <>
        <Header />
        <Routes>
          <Route path="/" element={<Banner />} />
          <Route path="/experts" element={<Experts />} />
          <Route path="/newsletter" element={<NewsLetter />} />
          <Route path="/plans" element={<Plans />} />
        </Routes>
        <Footer />
      </>
    </Router>
  );
}

export default App;
